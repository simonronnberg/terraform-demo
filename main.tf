# use aws
provider "aws" {
  region = "eu-west-1"
}

# ecs machine image
data "aws_ami" "ecs" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }
}

# network

resource "aws_vpc" "vpc" {
  cidr_block = "172.24.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
}

resource "aws_subnet" "public" {
  cidr_block = "172.24.16.0/24"
  vpc_id = "${aws_vpc.vpc.id}"
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true
}

# routing

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
}

resource "aws_route" "main" {
  destination_cidr_block = "0.0.0.0/0"
  route_table_id = "${aws_vpc.vpc.main_route_table_id}"
  gateway_id = "${aws_internet_gateway.igw.id}"
}

# load balancer

resource "aws_elb" "demo" {
  name = "demo"
  subnets = ["${aws_subnet.public.id}"]
  security_groups = ["${aws_security_group.sg.id}"]

  "listener" {
    instance_port = 8080
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }
}

resource "aws_security_group" "sg" {
  name = "sg"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

output "host" {
  value = "${aws_elb.demo.dns_name}"
}

# machine instances

resource "aws_autoscaling_group" "asg" {
  name = "instances"
  launch_configuration = "${aws_launch_configuration.lcg.id}"
  vpc_zone_identifier = ["${aws_subnet.public.id}"]

  min_size = 5
  max_size = 5
}

resource "aws_launch_configuration" "lcg" {
  image_id = "${data.aws_ami.ecs.id}"
  instance_type = "t2.micro"

  name_prefix = "nginx-"
  iam_instance_profile = "${aws_iam_instance_profile.ecs_instance.name}"
  security_groups = ["${aws_security_group.sg.id}"]

  user_data = "${data.template_file.userdata.rendered}"
  depends_on = ["aws_ecs_cluster.cluster"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_ecs_cluster" "cluster" {
  name = "nginx-cluster"
}

data "template_file" "userdata" {
  template = "${file("ecs_basic.sh")}"

  vars {
    cluster_name = "${aws_ecs_cluster.cluster.name}"
  }
}

# nginx on ecs

resource "aws_ecs_service" "nginx" {
  cluster = "${aws_ecs_cluster.cluster.id}"
  name = "nginx"
  desired_count = 5
  task_definition = "${aws_ecs_task_definition.nginx.arn}"
  iam_role = "${aws_iam_role.ecs_service_role.arn}"
  deployment_minimum_healthy_percent = 25

  load_balancer {
    elb_name = "${aws_elb.demo.name}"
    container_name = "nginx"
    container_port = 80
  }
}

resource "aws_ecs_task_definition" "nginx" {
  container_definitions = "${data.template_file.nginx.rendered}"
  family = "nginx"
}

data "template_file" "nginx" {
  template = "${file("nginx.json")}"
}
